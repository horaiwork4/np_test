<?php

use League\Route\Router;
use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use App\Http\Controller\BarcodeController;
use Laminas\Diactoros\ServerRequestFactory;
use Psr\Http\Message\ServerRequestInterface;
use League\Route\Strategy\ApplicationStrategy;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

require __DIR__ . '/vendor/autoload.php';


$request = ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);


// Routes
$router = new Router;
$router->map('GET', '/', function (ServerRequestInterface $request) : ResponseInterface {
    $response = new Response;
    $response->getBody()->write('TEST FOR NP');
    return $response;
});

$router
    ->group('/api', function ($router) {
        $router->map('GET', '/barcode', \App\Http\Controller\BarcodeController::class);
    });


$response = $router->dispatch($request);

(new SapiEmitter)->emit($response);
