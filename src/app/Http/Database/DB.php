<?php
namespace App\Http\Database;

use PDO;
use App\Http\Database\Connection;

class DB 
{
    private static $db;
    private static $connection;

    public static function DB()
    {
        self::$db = Connection::getInstance();
        self::$connection = self::$db->getConnection();

        return self::$connection;
    }


    /**
     * Undocumented function
     *
     * @param string $sql
     * @return void
     */
    public static function fetch($sql)
    {
        $stmt = self::DB()->prepare($sql);
        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Undocumented function
     *
     * @param string $sql
     * @return void
     */
    public static function fetchAll($sql)
    {
        $stmt = self::DB()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Undocumented function
     *
     * @param string $sql
     * @return void
     */
    public static function insert($sql)
    {
        $stmt = self::DB()->prepare($sql);
        return $stmt->execute();
    }
}