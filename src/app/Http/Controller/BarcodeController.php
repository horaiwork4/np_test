<?php

namespace App\Http\Controller;

use Carbon\Carbon;
use App\Http\Database\DB;
use Laminas\Diactoros\Response;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Diactoros\Response\TextResponse;
use Psr\Http\Message\ServerRequestInterface;

class BarcodeController
{
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $param = $request->getQueryParams();
        $codes = [];
        if (isset($param['code'])) {
            $codes = explode(',', $param['code']);
            // Проверить валидность кола
            $codes = $this->validCode($codes);
            // Получить массив из 1000 единиц
            $codes = array_slice($codes, 0, 1000);
        }

        return $this->toArray($codes);
    }

    /**
     * Найти код в базе
     *
     * @param  string $pass
     * @return array
     */
    private function getCodes($codes)
    {
        $codes = implode(",", $codes);
        $sql = "SELECT * FROM np_test.Test WHERE Number IN  ( $codes )";
        $codes = DB::fetchAll($sql);

        return $codes;
    }

    /**
     * Создание штрих-кода
     *
     * @param array $codes
     * @return array
     */
    private function getBarCodes($codes)
    {
        $generator = new BarcodeGeneratorPNG();

        foreach ($codes as $code) {
            if (file_exists('storage/barcode/' .$code->Number . '.png')) {
                $code->barCode = 'http://localhost/storage/barcode/' . $code->Number . '.png';
            } else {
                file_put_contents('storage/barcode/' . $code->Number . '.png', $generator->getBarcode($code->Number, $generator::TYPE_CODE_128, 3, 50));
                $code->barCode = 'http://localhost/storage/barcode/' . $code->Number . '.png';
            }
        }

        return $codes;
    }

    /**
     * Проверить является ли код числом
     *
     * @param array $codes
     * @return array
     */
    public function validCode($codes)
    {
        foreach ($codes as $key => $code) {
            if (!is_numeric($code)) {
                unset($codes[$key]);
            }
        }

        return $codes;
    }

    /**
     * Ответ на запрос
     *
     * @param array $codes
     * @return array
     */
    public function toArray($codes) : JsonResponse
    {
        $response = [
            'status' => 'success',
            'code' => '200',
            'data' => $this->getBarCodes($this->getCodes($codes)),
        ];

        return new JsonResponse($response);
    }
}
