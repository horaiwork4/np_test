<?php

namespace App\Http;

use Rareloop\Router\Router;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class Application
{
    protected $routes;

    public function __construct()
    {
        $this->routes =  new Router();
    }


    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        $path = $request->getPathInfo();
        $method = $request->getMethod();


        dd($this->routes->getRoutes()[0]);

        // Этот URL соответствует маршруту?
        if (array_key_exists($path, $this->routes)) {
            // выполняем замыкание
            $controller = $this->routes->all()[$path];

            $response = $controller();
        } else {
            // не было найдено соответствий, показываем ошибку.
            $response = new Response('Не найден!', Response::HTTP_NOT_FOUND);
        }

        return $response;
    }



    public function mapApiRoutes()
    {
        $this->routes->map(['GET'], '/api/auth', '\App\Http\Controller\AuthController@index');
        $this->routes->map(['GET'], '/api/auth/{id}', '\App\Http\Controller\AuthController@show' , function($params) {
             return $params->id;
        });
    }
}
